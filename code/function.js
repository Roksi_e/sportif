const modalTwo = document.querySelector(".parent-two"),
  cartCheck = createElement("div", "cart-check", "0"),
  divCart = document.querySelector(".div-cart"),
  goodButtons = document.querySelector(".buttons");

let circle = 0,
  productCard;

divCart.append(cartCheck);

function createElement(
  elementName,
  className = "",
  content = "",
  link = "",
  type = "",
  value = "",
  src = ""
) {
  const element = document.createElement(elementName);
  if (className !== "" || className !== null) {
    element.className = className;
  }
  if (content !== "" || content !== null) {
    element.textContent = content;
  }
  if (link !== "" || link !== null) {
    element.href = link;
  }
  if (type !== "" || type !== null) {
    element.type = type;
  }
  if (value !== "" || value !== null) {
    element.value = value;
  }
  if (src !== "" || src !== null) {
    element.src = src;
  }
  return element;
}

class SelectionBar {
  constructor(
    headSelect,
    divHeadSelect,
    divVectorSelect,
    vectorSelect,
    bodySelect
  ) {
    this.headSelect = headSelect;
    this.divHeadSelect = divHeadSelect;
    this.divVectorSelect = divVectorSelect;
    this.vectorSelect = vectorSelect;
    this.bodySelect = bodySelect;
    this.isVector = true;
  }
  createHeadSelectBar() {
    this.divVectorSelect.append(this.vectorSelect);
    this.headSelect.append(this.divHeadSelect, this.divVectorSelect);
    return this.headSelect;
  }
}

function createCircle(element, object) {
  const divCircle = createElement("div", "div-circle");
  for (let i = 0; i < 5; i++) {
    circle = createElement("div", "circle");
    divCircle.append(circle);
  }
  element.append(divCircle);
  const allStars = divCircle.querySelectorAll(".circle");
  allStars.forEach((star, i) => {
    i = `${object.rate}`;
    allStars.forEach((star, j) => {
      if (i >= j + 1) {
        star.classList.add("active-circle");
      } else {
        star.classList.remove("active-circle");
      }
    });
  });

  return element;
}

function createCart(element, key) {
  const productCart = createElement("div", "sale-input"),
    productBtn = createElement(
      "input",
      "sale-btn",
      undefined,
      undefined,
      "button",
      "ADD TO CART"
    ),
    cartImg = createElement(
      "img",
      "bags",
      undefined,
      undefined,
      undefined,
      undefined,
      "../img/vector/bags.png"
    );
  productBtn.setAttribute("data-articul", key);
  productCart.append(productBtn, cartImg);
  element.append(productCart);
  return element;
}

function createProductPrice(element, object, key) {
  const productPrice = createElement("div", "sale", "As low as"),
    productSpan = createElement(
      "span",
      "sale-price",
      ` $${object.price.toFixed(2)}`
    );
  productPrice.append(productSpan);
  element.append(productPrice);
  return element;
}

function createProductColor(element, object, key) {
  const productColorCard = createElement("div", "product-color-card");
  for (let color in object.colors) {
    let divColor = createElement("div", "short-color-select");
    divColor.style.backgroundColor = `${object.colors[color]}`;
    productColorCard.append(divColor);
  }
  element.append(productColorCard);
  return element;
}

function createProductSize(element, object, key) {
  const productSizeCard = createElement("div", "product-size-card");
  for (let size in object.sizes) {
    let divSize = createElement("div", "short-size-select");
    divSize.textContent = `${object.sizes[size]}`;
    productSizeCard.append(divSize);
  }
  element.append(productSizeCard);
  return element;
}

function createProductTop(element, object, key) {
  const cardTopTotal = createElement("div", "product-top-total"),
    productCardImg = createElement("div", "product-card-img"),
    productImg = createElement(
      "img",
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      `${object.image[0]}`
    ),
    productTitle = createElement("div", "card-title", `${object.name}`);
  productCardImg.append(productImg);
  cardTopTotal.append(productCardImg, productTitle);
  element.append(cardTopTotal);

  return element;
}

function clearActiveBtn(array) {
  array.forEach((item) => {
    item.classList.remove("active");
  });
}

function clearActiveImage(array) {
  array.forEach((item) => {
    item.classList.remove("image-active");
  });
}

function clearPage(array) {
  array.forEach((item) => {
    item.innerHTML = "";
  });
}

function removeList(array) {
  array.forEach((item) => {
    item.remove();
  });
}

function showProduct(item) {
  productCard = createElement("div", "card");
  productCard.setAttribute("data-articul", `${item[0]}`);
  createProductTop(productCard, item[1], item[0]);
  createCircle(productCard, item[1]);
  createProductPrice(productCard, item[1], item[0]);
  createProductColor(productCard, item[1], item[0]);
  createCart(productCard, item[0]);

  touchCard(item, productCard);

  return productCard;
}

function touchCard(item, element) {
  element.addEventListener("click", (e) => {
    if (e.target.className !== "sale-btn") {
      dataPage.push(item[0], item[1]);
      localStorage.productPage = JSON.stringify(dataPage);
      document.location.assign("../shorts/index.html");
    } else if (e.target.className === "sale-btn") {
      modalTwo.style.display = "flex";
      let key = e.target.dataset.articul;
      if (localStorage.customerOrder) {
        dataCart = JSON.parse(localStorage.customerOrder);
        if (dataCart[key] !== undefined) {
          dataCart[key].count++;
        } else {
          item[1].sizes.splice(1, item[1].sizes.length - 1);
          item[1].colors.splice(1, item[1].colors.length - 1);
          dataCart[key] = item[1];
          dataCart[key].count = 1;
        }
      } else {
        if (dataCart[key] !== undefined) {
          dataCart[key].count++;
        } else {
          item[1].sizes.splice(1, item[1].sizes.length - 1);
          item[1].colors.splice(1, item[1].colors.length - 1);
          dataCart[key] = item[1];
          dataCart[key].count = 1;
        }
      }
      localStorage.customerOrder = JSON.stringify(dataCart);
      checkCart();
    }
  });
}

function sizeFilter(array, value) {
  return array.filter((el) => {
    return el[1].sizes.includes(value);
  });
}

function colorFilter(array, value) {
  return array.filter((el) => {
    return el[1].colors.includes(value);
  });
}

goodButtons.firstElementChild.addEventListener("click", (e) => {
  modalCartPoint.innerHTML = "";
  modalTwo.style.display = "none";
});

goodButtons.lastElementChild.addEventListener("click", (e) => {
  modalCartPoint.innerHTML = "";
  dataCart = JSON.parse(localStorage.customerOrder);
  modalTwo.style.display = "none";
  cartBottom.style.display = "flex";
  viewCart(dataCart);
  showPrice(dataCart);
  cartEmpty.remove();
  modalCartTotal.append(modalCartPoint);
  modal.classList.add("active-modal");
});

function checkCart() {
  if (localStorage.customerOrder) {
    dataCart = JSON.parse(localStorage.customerOrder);
    return (cartCheck.textContent = `${Object.keys(dataCart).length}`);
  }
}
