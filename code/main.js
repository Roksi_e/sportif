const titleInput = document.querySelector(".title-input");

let cardSale = document.querySelector(".card-total");

const viewProductSale = Object.entries(productSale);
viewProductSale.map((item, index) => {
  if (index >= 0 && index <= 4) {
    productCard = createElement("div", "card");
    createProductTop(productCard, item[1], item[0]);
    createCircle(productCard, item[1]);
    createProductPrice(productCard, item[1], item[0]);
    createCart(productCard, item[0]);
    cardSale.prepend(productCard);
  }
  touchCard(item, productCard);
});

function createCart(element, key) {
  const productCart = createElement("div", "sale-input"),
    productBtn = createElement(
      "input",
      "sale-btn",
      undefined,
      undefined,
      "button",
      "ADD TO CART"
    ),
    cartImg = createElement(
      "img",
      "bags",
      undefined,
      undefined,
      undefined,
      undefined,
      "./img/vector/bags.png"
    );
  productBtn.setAttribute("data-articul", key);
  productCart.append(productBtn, cartImg);
  element.append(productCart);
  return element;
}

checkCart();

window.addEventListener("DOMContentLoaded", () => {
  titleInput.addEventListener("click", () => {
    document.location.assign("./catalog/index.html");
  });
});
