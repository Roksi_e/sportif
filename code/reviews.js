const [...divCircleRew] = document.querySelectorAll('.div-circle-rew'),
	commentsPage = document.querySelector('.users-reviews-total'),
	modalThree = document.querySelector('.parent-three')

const bottomRevMain = createElement('div', 'bottom-main-rev'),
	bottomRevBtn = [
		createElement('bottom', 'btn-bottom-main active', '1'),
		createElement('bottom', 'btn-bottom-main ', '2')
	],
	commentsPages = [createElement('div', 'users-reviews'), createElement('div', 'users-reviews')],
	btnRevRight = createElement('bottom', 'btn-bottom-main base'),
	btnRevRightImg = createElement('img')

const validate = (r, v) => r.test(v)

btnRevRightImg.src = '../img/vector/right-bordo.png'
btnRevRight.append(btnRevRightImg)

if (localStorage.userCom) {
	dataUserRew = JSON.parse(localStorage.userCom)
}

divCircleRew.forEach(item => {
	const circleDivRew = createElement('div', 'circle-div-rew')
	for (let i = 0; i < 5; i++) {
		let circleRew = createElement('button', 'circle-rew')
		circleRew.innerHTML = '&#9734'
		circleRew.setAttribute('data-count', i + 1)
		circleDivRew.append(circleRew)
	}
	item.append(circleDivRew)
	const allStars = circleDivRew.querySelectorAll('.circle-rew')
	allStars.forEach((star, i) => {
		star.addEventListener('click', e => {
			let current_star_level = i + 1
			allStars.forEach((star, j) => {
				if (current_star_level >= j + 1) {
					star.innerHTML = '&#9733'
					star.classList.add('active')
				} else {
					star.innerHTML = '&#9734'
					star.classList.remove('active')
				}
			})
			if (e.target.parentElement.parentElement.previousElementSibling.textContent === 'OVERALL') {
				dataUserRew['OVERALL'] = `${e.target.dataset.count}`
			} else if (e.target.parentElement.parentElement.previousElementSibling.textContent === 'COMFORT') {
				dataUserRew['COMFORT'] = `${e.target.dataset.count}`
			} else if (e.target.parentElement.parentElement.previousElementSibling.textContent === 'QUALITY') {
				dataUserRew['QUALITY'] = `${e.target.dataset.count}`
			} else if (e.target.parentElement.parentElement.previousElementSibling.textContent === 'FIT') {
				dataUserRew['FIT'] = `${e.target.dataset.count}`
			}
		})
	})
})

const [...userReviews] = document.querySelectorAll('.user-review-grid input'),
	userText = document.querySelector('#textarea')

function createCircleRev(element, value) {
	const divCircle = createElement('div', 'div-circle')
	for (let i = 0; i < 5; i++) {
		circle = createElement('div', 'circle')
		divCircle.append(circle)
	}
	element.append(divCircle)
	const allStars = divCircle.querySelectorAll('.circle')
	allStars.forEach((star, i) => {
		i = `${value}`
		allStars.forEach((star, j) => {
			if (i >= j + 1) {
				star.classList.add('active-circle')
			} else {
				star.classList.remove('active-circle')
			}
		})
	})

	return element
}

const createComment = ({ comment, data, name, surname, COMFORT, FIT, OVERALL, QUALITY }) => {
	const elTotal = createElement('div', 'total'),
		elLeft = createElement('div', 'left'),
		elLeftTitleCom = createElement('div', 'left-title'),
		elTitleCom = createElement('div', 'title', 'COMFORT'),
		elLeftTitleCircleCom = createElement('div', 'circle-comment'),
		elLeftTitleFit = createElement('div', 'left-title'),
		elTitleFit = createElement('div', 'title', 'FIT'),
		elLeftTitleCircleFit = createElement('div', 'circle-comment'),
		elLeftTitleOver = createElement('div', 'left-title'),
		elTitleOver = createElement('div', 'title', 'OVERALL'),
		elLeftTitleCircleOver = createElement('div', 'circle-comment'),
		elLeftTitleQual = createElement('div', 'left-title'),
		elTitleQual = createElement('div', 'title', 'QUALITY'),
		elLeftTitleCircleQual = createElement('div', 'circle-comment'),
		elRight = createElement('div', 'right'),
		elComment = createElement('div', 'comment'),
		elName = createElement('div', 'name')
	////?
	const elImgCom = createCircleRev(elLeftTitleCircleCom, COMFORT),
		elImgFit = createCircleRev(elLeftTitleCircleFit, FIT),
		elImgOver = createCircleRev(elLeftTitleCircleOver, OVERALL),
		elImgQual = createCircleRev(elLeftTitleCircleQual, QUALITY)

	elComment.textContent = comment
	elName.textContent = `By ${name} ${surname} - Posted on ${data}`

	elLeft.append(elLeftTitleCom, elLeftTitleFit, elLeftTitleOver, elLeftTitleQual)

	elLeftTitleCom.append(elTitleCom, elLeftTitleCircleCom)
	elLeftTitleFit.append(elTitleFit, elLeftTitleCircleFit)
	elLeftTitleOver.append(elTitleOver, elLeftTitleCircleOver)
	elLeftTitleQual.append(elTitleQual, elLeftTitleCircleQual)
	elRight.append(elComment, elName)
	elTotal.append(elLeft, elRight)

	return elTotal
}

const comments = dataUserRews.map(el => {
	return createComment(el)
})

function changeComments(array) {
	array.forEach((item, index) => {
		if (index >= 0 && index <= 5) {
			commentsPages[0].append(item)
		}
		if (index > 5) {
			commentsPages[1].append(item)
		}
	})
}

window.addEventListener('DOMContentLoaded', () => {
	commentsPage.append(commentsPages[0])
	commentsPage.after(bottomRevMain)
	bottomRevMain.append(...bottomRevBtn, btnRevRight)
	blockMain.prepend(modalThree)

	changeComments(comments)

	userReviews.forEach(item => {
		if (item.type === 'button') {
			item.addEventListener('click', e => {
				if (dataUserRew.name !== '' && dataUserRew.surname !== '') {
					dataUserRew.data = `${new Date().getMonth() + 1}/${new Date().getDate()}/${new Date()
						.getFullYear()
						.toString()
						.slice(2)}`
					dataUserRews.unshift(dataUserRew)
					localStorage.userCom = JSON.stringify(dataUserRew)
					comments.unshift(createComment(dataUserRew))
					changeComments(comments)
					commentsPages[0].prepend(comments[0])
					modalThree.style.display = 'flex'
					setTimeout(function () {
						modalThree.style.display = 'none'
						userReviews[0].value = ''
						userReviews[1].value = ''
						userText.value = ''
					}, 2000)
				} else {
					console.error('error')
				}
			})
		} else {
			item.addEventListener('change', e => {
				if (e.target.id === 'name' && validate(/([A-z]{2,})/i, e.target.value)) {
					dataUserRew.name = e.target.value
				} else if (e.target.id === 'surname' && validate(/([A-z]{1,})/g, e.target.value)) {
					dataUserRew.surname = e.target.value
				} else {
					e.target.value = 'Enter correctly'
					e.target.style.color = 'red'
					setTimeout(function () {
						e.target.value = ''
						e.target.style.color = ''
					}, 2000)
				}
			})
		}
	})

	userText.addEventListener('change', e => {
		dataUserRew.comment = e.target.value
	})

	bottomRevBtn.forEach(item => {
		item.addEventListener('click', e => {
			clearActiveBtn(bottomRevBtn)
			item.classList.add('active')
			if (e.target.textContent === '1') {
				commentsPages[1].remove()
				commentsPage.append(commentsPages[0])
			}
			if (e.target.textContent === '2') {
				commentsPages[0].remove()
				commentsPage.append(commentsPages[1])
			}
		})
	})

	btnRevRight.addEventListener('click', e => {
		clearActiveBtn(bottomRevBtn)
		bottomRevBtn[1].classList.add('active')
		commentsPages[0].remove()
		commentsPage.append(commentsPages[1])
	})
})
