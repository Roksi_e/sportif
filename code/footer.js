const blockFooter = document.querySelector("footer"),
  footer = document.querySelector(".footer"),
  date = `©${new Date().getFullYear()} Sportif Mailorder. All Rights Reserved.`;

const footerInfo = createElement("div", "footer-bordo"),
  footerEnd = createElement("div", "footer-end", date),
  footerBtn = createElement("button", undefined, "SUBSCRIBE"),
  footerInput = createElement(
    "input",
    "footer-input",
    undefined,
    undefined,
    "email"
  ),
  footerChild = [
    createElement("div", "footer-left"),
    createElement("div", "footer-midle"),
    createElement("div", "footer-midle"),
    createElement("div", "footer-right"),
  ],
  footerLeft = [
    createElement("div", "footer-title", "ORDERING ONLINE"),
    createElement("ul", "footer-ul"),
  ],
  footerLeftMidle = [
    createElement("div", "footer-title", "ABOUT SPORTIF"),
    createElement("ul", "footer-ul"),
  ],
  footerRightMidle = [
    createElement("div", "footer-title", "QUICK LINKS"),
    createElement("ul", "footer-ul"),
  ],
  footerRight = [
    createElement("div", "footer-title", "GET TO KNOW US"),
    createElement("ul", "footer-ul"),
    createElement("div", "footer-div-input"),
  ],
  footerOne = [
    "Account Login",
    "Our Guarantee",
    "Sportif Stretch Guide",
    "Size Chart & Sizing Information",
    "Hemming Information",
    "Ordering & Payment",
    "Shipping Information",
    "Returns",
  ],
  footerTwo = [
    "COVID-19 Response",
    "History",
    "Legacy",
    "Good Sam Program",
    "Privacy & Security",
    "Terms & Conditions",
    " Careers",
  ],
  footerThree = ["FAQs", "Shop Online Catalog", "Contact Us"],
  footerFour = [
    "Sign up for our weekly newsletter and get a 10% off coupon by email for your first order!",
    "",
    "Sign Up for Our Newsletter:",
  ],
  lisOne = footerOne.map((item) => {
    const li = createElement("li", "footer-one-item");
    const a = createElement("a", undefined, item, "#");
    li.append(a);
    return li;
  }),
  lisTwo = footerTwo.map((item) => {
    const li = createElement("li", "footer-one-item");
    const a = createElement("a", undefined, item, "#");
    li.append(a);
    return li;
  }),
  lisThree = footerThree.map((item) => {
    const li = createElement("li", "footer-one-item");
    const a = createElement("a", undefined, item, "#");
    li.append(a);
    return li;
  }),
  lisFour = footerFour.map((item) => {
    const li = createElement("li", "footer-one-item", item);
    return li;
  }),
  adressSuccess = createElement(
    "div",
    "adress-success",
    "Thank you! You subscribed"
  ),
  adressError = createElement(
    "div",
    "adress-error",
    "Please, reverse the correctness of the entered data"
  );

footer.append(footerInfo, footerEnd);
footerInfo.append(...footerChild);
footerChild[0].append(...footerLeft);
footerLeft[1].append(...lisOne);
footerChild[1].append(...footerLeftMidle);
footerLeftMidle[1].append(...lisTwo);
footerChild[2].append(...footerRightMidle);
footerRightMidle[1].append(...lisThree);
footerChild[3].append(...footerRight);
footerRight[1].append(...lisFour);
footerRight[2].append(adressSuccess, adressError, footerInput, footerBtn);
footerInput.placeholder = "EMAIL ADDRESS";

window.addEventListener("DOMContentLoaded", () => {
  blockFooter.append(footer);

  footerInput.addEventListener("change", (evt) => {
    footerBtn.addEventListener("click", () => {
      let result;
      const dataEmail = [];
      if (
        (result = /^[A-z_0-9.]+@[A-z-]+\.[A-z]{1,4}\.?[A-z]*$/.test(
          evt.target.value
        ))
      ) {
        adressSuccess.style.display = "block";
        setTimeout(function () {
          adressSuccess.style.display = "none";
          evt.target.value = "";
        }, 5000);
        dataEmail.push(evt.target.value);
      } else {
        adressError.style.display = "block";
        setTimeout(function () {
          adressError.style.display = "none";
        }, 5000);
        evt.target.value = "";
      }
    });
  });
});
