const selectBar = [],
	sizeSelect = [30, 32, 34, 36, 38, 40, 42, 44, 46],
	colorSelect = [
		'#000000',
		'#744524',
		'#113EB1',
		'#549554',
		'#808080',
		'#E59500',
		'#FFFFFF',
		'#d9d9d9',
		'#262673',
		'#a6a6a6',
		'#26260d',
		'#c2c2a3',
		'#ffffb3',
		'#7599bd',
		'#996633',
		'#adad85',
		'#576675',
		'#5c5c3d'
	],
	bottomsSelect = ['Short', 'Cargo', 'Plain Front', 'Denim Jean', 'Chino', 'Swim', 'Pleated Front'],
	shortsList = {
		670100: {
			name: "Sportif's Original Short",
			image: ['../img/shorts/image10.png', '../img/shorts/image50.png'],
			price: 67.0,
			colors: ['#808080', '#E59500', '#FFFFFF', '#d9d9d9', '#26260d'],
			sizes: [30, 32, 34, 36, 38, 40, 42, 44, 46],
			rate: 5,
			reviews: 93
		},
		670101: {
			name: "Sportif's Hatteras Short",
			image: ['../img/shorts/image12.png'],
			price: 54.99,
			colors: ['#000000', '#744524', '#c2c2a3', '#ffffb3', '#7599bd', '#996633', '#5c5c3d'],
			sizes: [32, 34, 36, 42, 44, 46],
			rate: 5,
			reviews: 80
		},
		670102: {
			name: "Sportif's Tidewater Short",
			image: ['../img/shorts/image13.png'],
			price: 54.99,
			colors: ['#000000', '#744524', '#113EB1', '#adad85', '#576675', '#5c5c3d', ,],
			sizes: [32, 34, 36, 38, 40, 44, 46],
			rate: 5,
			reviews: 90
		},
		670103: {
			name: "Sportif's Lauderdale Short",
			image: ['../img/shorts/image14.png'],
			price: 67.0,
			colors: ['#000000', '#c2c2a3', '#ffffb3', '#adad85', '#576675', '#5c5c3d'],
			sizes: [36, 38, 40, 42, 44, 46],
			rate: 4,
			reviews: 100
		},
		670104: {
			name: "Captain's Short",
			image: ['../img/shorts/image20.png'],
			price: 67.0,
			colors: ['#000000', '#744524', '#113EB1', '#ffffb3', '#5c5c3d'],
			sizes: [30, 32, 34],
			rate: 4,
			reviews: 97
		},
		670105: {
			name: 'Galapagos Plain Short',
			image: ['../img/shorts/image24.png'],
			price: 34.99,
			colors: ['#000000', '#744524', '#7599bd', '#996633', '#adad85', '#576675', '#5c5c3d'],
			sizes: [32, 44, 46],
			rate: 5,
			reviews: 60
		},
		670106: {
			name: 'Galapagos Pleated Short',
			image: ['../img/shorts/image32.png'],
			price: 34.99,
			colors: ['#000000', '#744524', '#113EB1', '#d9d9d9', '#576675', '#5c5c3d'],
			sizes: [30, 36, 38, 40, 42, 46],
			rate: 5,
			reviews: 65
		},
		670107: {
			name: "Sportif's Tidewater Denim Cargo Short",
			image: ['../img/shorts/image34.png'],
			price: 69.0,
			colors: ['#000000', '#262673'],
			sizes: [44, 46],
			rate: 5,
			reviews: 40
		},
		670108: {
			name: "Marchal's Hatteras Short",
			image: ['../img/shorts/image36.png'],
			price: 40.99,
			colors: ['#E59500', '#FFFFFF', '#d9d9d9', '#262673', '#a6a6a6', '#adad85', '#5c5c3d'],
			sizes: [46],
			rate: 5,
			reviews: 93
		},
		670109: {
			name: 'Ecoths Ashcroft Short',
			image: ['../img/shorts/image44.png'],
			price: 41.99,
			colors: ['#000000', '#744524', '#113EB1', '#549554', '#FFFFFF', '#d9d9d9'],
			sizes: [30, 32, 34, 36, 38, 40, 42, 44, 46],
			rate: 5,
			reviews: 93
		},
		670110: {
			name: 'Ecoths Gannon Short',
			image: ['../img/shorts/image45.png'],
			price: 42.99,
			colors: ['#000000', '#262673'],
			sizes: [42, 44, 46],
			rate: 5,
			reviews: 20
		},
		670111: {
			name: "Sportif's Good Short",
			image: ['../img/shorts/1.png'],
			price: 67.0,
			colors: ['#000000'],
			sizes: [30, 32, 34, 36, 38, 40, 42, 44, 46],
			rate: 5,
			reviews: 93
		},
		670112: {
			name: "Sportif's Best Short",
			image: ['../img/shorts/2.png'],
			price: 54.99,
			colors: ['#000000', '#FFFFFF', '#d9d9d9', '#adad85', '#576675', '#5c5c3d'],
			sizes: [30, 32, 34, 36, 42, 44, 46],
			rate: 5,
			reviews: 80
		},
		670113: {
			name: "Sportif's Tider Short",
			image: ['../img/shorts/3.png'],
			price: 54.99,
			colors: ['#adad85', '#576675', '#5c5c3d'],
			sizes: [30, 32, 34, 36, 38, 40, 44, 46],
			rate: 5,
			reviews: 90
		},
		670114: {
			name: "Sportif's Lauder Short",
			image: ['../img/shorts/4.png'],
			price: 67.0,
			colors: ['#744524', '#113EB1', '#549554', '#adad85', '#576675'],
			sizes: [34, 36, 38, 40, 42, 44, 46],
			rate: 3,
			reviews: 100
		},
		670115: {
			name: "Captain's Short",
			image: ['../img/shorts/5.png'],
			price: 67.0,
			colors: ['#000000', '#576675', '#5c5c3d'],
			sizes: [30, 32, 34],
			rate: 5,
			reviews: 97
		},
		670116: {
			name: 'Gala Plain Short',
			image: ['../img/shorts/6.png'],
			price: 34.99,
			colors: ['#000000', '#744524'],
			sizes: [30, 32, 44, 46],
			rate: 1,
			reviews: 60
		},
		670117: {
			name: 'Gala Pleated Short',
			image: ['../img/shorts/7.png'],
			price: 34.99,
			colors: ['#000000', '#744524', '#113EB1'],
			sizes: [30, 36, 38, 40, 42, 46],
			rate: 2,
			reviews: 65
		},
		670118: {
			name: "Sportif's  Denim Cargo Short",
			image: ['../img/shorts/8.png'],
			price: 69.0,
			colors: ['#000000', '#744524', '#113EB1', '#adad85', '#576675', '#5c5c3d'],
			sizes: [44, 46],
			rate: 3,
			reviews: 40
		},
		670119: {
			name: 'March Hatteras Short',
			image: ['../img/shorts/9.png'],
			price: 40.99,
			colors: ['#000000', '#adad85', '#576675', '#5c5c3d'],
			sizes: [46],
			rate: 5,
			reviews: 93
		},
		670120: {
			name: 'Good Short',
			image: ['../img/shorts/10.png'],
			price: 67.0,
			colors: ['#000000', '#744524'],
			sizes: [30, 32, 34, 36, 38, 40, 42, 44, 46],
			rate: 5,
			reviews: 93
		},
		670121: {
			name: "Sportif's Short",
			image: ['../img/shorts/11.png'],
			price: 54.99,
			colors: ['#000000', '#FFFFFF'],
			sizes: [30, 32, 34, 36, 42, 44, 46],
			rate: 5,
			reviews: 80
		},
		670122: {
			name: 'Tider Short',
			image: ['../img/shorts/12.png'],
			price: 54.99,
			colors: ['#adad85', '#576675', '#5c5c3d'],
			sizes: [30, 32, 34, 36, 38, 40, 44, 46],
			rate: 5,
			reviews: 90
		},
		670123: {
			name: "Sportif's Laud Short",
			image: ['../img/shorts/13.png'],
			price: 67.0,
			colors: ['#744524', '#113EB1', '#549554', '#adad85', '#576675'],
			sizes: [34, 36, 38, 40, 42, 44, 46],
			rate: 5,
			reviews: 100
		},
		670124: {
			name: 'Cap Short',
			image: ['../img/shorts/14.png'],
			price: 67.0,
			colors: ['#000000', '#576675', '#5c5c3d'],
			sizes: [30, 32, 34],
			rate: 5,
			reviews: 97
		},
		670125: {
			name: 'Plain Short',
			image: ['../img/shorts/15.png'],
			price: 34.99,
			colors: ['#000000', '#744524'],
			sizes: [30, 32, 44, 46],
			rate: 5,
			reviews: 60
		},
		670126: {
			name: ' Pleated Short',
			image: ['../img/shorts/16.png'],
			price: 34.99,
			colors: ['#000000', '#744524', '#113EB1'],
			sizes: [30, 36, 38, 40, 42, 46],
			rate: 5,
			reviews: 65
		},
		670127: {
			name: "Sportif's Cargo Short",
			image: ['../img/shorts/17.png'],
			price: 69.0,
			colors: ['#000000', '#744524', '#113EB1', '#adad85', '#576675', '#5c5c3d'],
			sizes: [44, 46],
			rate: 5,
			reviews: 40
		},
		670128: {
			name: 'March Hatter Short',
			image: ['../img/shorts/18.png'],
			price: 40.99,
			colors: ['#000000', '#adad85', '#576675', '#5c5c3d'],
			sizes: [46],
			rate: 5,
			reviews: 93
		},
		670129: {
			name: 'Ecoths Ashc Short',
			image: ['../img/shorts/19.png'],
			price: 41.99,
			colors: ['#000000'],
			sizes: [30, 32, 34, 36, 38, 40, 42, 44, 46],
			rate: 5,
			reviews: 93
		},
		670130: {
			name: "Sportif's Lucky Short",
			image: ['../img/shorts/20.png'],
			price: 67.0,
			colors: ['#000000'],
			sizes: [30, 32, 34, 36, 38, 40, 42, 44, 46],
			rate: 5,
			reviews: 93
		},
		670131: {
			name: "Sportif's VIP Short",
			image: ['../img/shorts/21.png'],
			price: 54.99,
			colors: ['#000000', '#FFFFFF', '#d9d9d9', '#adad85', '#576675', '#5c5c3d'],
			sizes: [30, 32, 34, 36, 42, 44, 46],
			rate: 5,
			reviews: 80
		},
		670132: {
			name: "Sportif's Tinder Short",
			image: ['../img/shorts/22.png'],
			price: 54.99,
			colors: ['#adad85', '#576675', '#5c5c3d'],
			sizes: [30, 32, 34, 36, 38, 40, 44, 46],
			rate: 5,
			reviews: 90
		},
		670133: {
			name: "Sportif's Lauder Best Short",
			image: ['../img/shorts/23.png'],
			price: 67.0,
			colors: ['#744524', '#113EB1', '#549554', '#adad85', '#576675'],
			sizes: [34, 36, 38, 40, 42, 44, 46],
			rate: 5,
			reviews: 100
		},
		670134: {
			name: "Captain's Morgan Short",
			image: ['../img/shorts/24.png'],
			price: 67.0,
			colors: ['#000000', '#576675', '#5c5c3d'],
			sizes: [30, 32, 34],
			rate: 5,
			reviews: 97
		},
		670135: {
			name: 'Gala Plaintich Short',
			image: ['../img/shorts/25.png'],
			price: 34.99,
			colors: ['#000000', '#744524'],
			sizes: [30, 32, 44, 46],
			rate: 5,
			reviews: 60
		},
		670136: {
			name: 'Gala Short',
			image: ['../img/shorts/26.png'],
			price: 34.99,
			colors: ['#000000', '#744524', '#113EB1'],
			sizes: [30, 36, 38, 40, 42, 46],
			rate: 5,
			reviews: 65
		},
		670137: {
			name: "Sportif's Super Short",
			image: ['../img/shorts/27.png'],
			price: 69.0,
			colors: ['#000000', '#744524', '#113EB1', '#adad85', '#576675', '#5c5c3d'],
			sizes: [44, 46],
			rate: 5,
			reviews: 40
		},
		670138: {
			name: 'March Hatt Short',
			image: ['../img/shorts/28.png'],
			price: 40.99,
			colors: ['#000000', '#adad85', '#576675', '#5c5c3d'],
			sizes: [46],
			rate: 5,
			reviews: 93
		},
		670139: {
			name: 'Good Brash Short',
			image: ['../img/shorts/29.png'],
			price: 67.0,
			colors: ['#000000', '#744524'],
			sizes: [30, 32, 34, 36, 38, 40, 42, 44, 46],
			rate: 5,
			reviews: 93
		},
		670140: {
			name: "Sportif's Cool Short",
			image: ['../img/shorts/30.png'],
			price: 54.99,
			colors: ['#000000', '#FFFFFF'],
			sizes: [30, 32, 34, 36, 42, 44, 46],
			rate: 5,
			reviews: 80
		},
		670141: {
			name: 'Tider Liber Short',
			image: ['../img/shorts/31.png'],
			price: 54.99,
			colors: ['#adad85', '#576675', '#5c5c3d'],
			sizes: [30, 32, 34, 36, 38, 40, 44, 46],
			rate: 5,
			reviews: 90
		},
		670142: {
			name: "Sportif's Goom Short",
			image: ['../img/shorts/32.png'],
			price: 67.0,
			colors: ['#744524', '#113EB1', '#549554', '#adad85', '#576675'],
			sizes: [34, 36, 38, 40, 42, 44, 46],
			rate: 5,
			reviews: 100
		},
		670143: {
			name: 'Carpach Short',
			image: ['../img/shorts/33.png'],
			price: 67.0,
			colors: ['#000000', '#576675', '#5c5c3d'],
			sizes: [30, 32, 34],
			rate: 5,
			reviews: 97
		},
		670144: {
			name: 'Plain Well Short',
			image: ['../img/shorts/34.png'],
			price: 34.99,
			colors: ['#000000', '#744524'],
			sizes: [30, 32, 44, 46],
			rate: 5,
			reviews: 60
		},
		670145: {
			name: ' Pleated Good Short',
			image: ['../img/shorts/35.png'],
			price: 34.99,
			colors: ['#000000', '#744524', '#113EB1'],
			sizes: [30, 36, 38, 40, 42, 46],
			rate: 5,
			reviews: 65
		},
		670146: {
			name: "Sportif's Doom Short",
			image: ['../img/shorts/36.png'],
			price: 69.0,
			colors: ['#000000', '#744524', '#113EB1', '#adad85', '#576675', '#5c5c3d'],
			sizes: [44, 46],
			rate: 5,
			reviews: 40
		},
		670147: {
			name: 'March Liber Short',
			image: ['../img/shorts/37.png'],
			price: 40.99,
			colors: ['#000000', '#adad85', '#576675', '#5c5c3d'],
			sizes: [46],
			rate: 5,
			reviews: 93
		},
		670148: {
			name: 'Ecoths Lux Short',
			image: ['../img/shorts/MAIN.png'],
			price: 41.99,
			colors: ['#000000'],
			sizes: [30, 32, 34, 36, 38, 40, 42, 44, 46],
			rate: 5,
			reviews: 93
		},
		670149: {
			name: 'Gannon Short',
			image: ['../img/shorts/MAIN1.png'],
			price: 42.99,
			colors: ['#000000'],
			sizes: [42, 44, 46],
			rate: 5,
			reviews: 20
		}
	}

const productSale = {
	223344: {
		name: "Marchal's Original Cargo Short",
		image: ['../img/sale/CargoShort.png'],
		price: 40.99,
		colors: ['#ffffb3'],
		sizes: [32, 42],
		rate: 5,
		reviews: 20
	},
	332266: {
		name: 'Inlet Original Short',
		image: ['../img/sale/Short.png'],
		price: 38.99,
		colors: ['#113EB1'],
		sizes: [44, 46],
		rate: 5,
		reviews: 20
	},
	223399: {
		name: 'TGIF Pant',
		image: ['../img/sale/Pant.png'],
		price: 40.99,
		colors: ['#FFFFFF'],
		sizes: [36, 44],
		rate: 4,
		reviews: 20
	},
	119933: {
		name: 'Inlet Original Pant',
		image: ['../img/sale/OriginPant.png'],
		price: 52.99,
		colors: ['#ffffb3'],
		sizes: [36, 46],
		rate: 4,
		reviews: 20
	}
}

const searchShot = Object.entries(shortsList)
const searchBase = Object.values(shortsList)
let userProduct = shortsList
let userShowProduct = searchShot,
	dataCart = {},
	dataCustomer = [],
	dataPage = [],
	userPage = [],
	userSearch = {}
